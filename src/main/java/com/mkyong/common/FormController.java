package com.mkyong.common;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.awt.event.ActionEvent;
import java.io.Serializable;

/**
 * Created by x on 2017-07-23.
 */
@ManagedBean(name = "formController", eager = true)
@ApplicationScoped
public class FormController implements Serializable {

    private String ownerName;
    private String ownerSurname;
    private int ownerPhone;
    private String dogName;
    private int dogAge;

    private ApControler apControler = new ApControler();

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerSurname() {
        return ownerSurname;
    }

    public void setOwnerSurname(String ownerSurname) {
        this.ownerSurname = ownerSurname;
    }

    public int getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(int ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    public String getDogName() {
        return dogName;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public int getDogAge() {
        return dogAge;
    }

    public void setDogAge(int dogAge) {
        this.dogAge = dogAge;
    }

    public void saveOwner(){
        Owner owner = new Owner();

        owner.setName(this.getOwnerName());
        owner.setSurname(this.getOwnerSurname());
        owner.setPhoneNumber(this.getOwnerPhone());


        apControler.getOwners().add(owner);
        System.out.println(owner);
        System.out.println("Owner saved.");
    }
    public void saveDog(){
        Dog dog = new Dog();
        dog.setName(this.getDogName());
        dog.setAge(this.getDogAge());

        apControler.getDog().add(dog);
        System.out.println("Dog saved.");

    }

    public void saveInformation(){
        saveOwner();
        saveDog();
    }
}
