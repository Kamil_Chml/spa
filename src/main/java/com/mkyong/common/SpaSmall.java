package com.mkyong.common;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by x on 2017-07-20.
 */
@ManagedBean
@SessionScoped
public class SpaSmall implements Serializable {

    private int strzyzenie = 50;
    private int kapiel = 30;
    private int spacer = 10;
    private int bieganie = 20;
    private int dieta = 100;
    private List<Integer> checkedValues;
    private List<Integer> availableValues;

    @PostConstruct
    public void init(){
        availableValues = new ArrayList<>();
        availableValues.add(strzyzenie);
        availableValues.add(kapiel);
        availableValues.add(spacer);
        availableValues.add(bieganie);
        availableValues.add(dieta);
    }
    public List<Integer> submit() {
        return checkedValues;
    }


    public List<Integer> getCheckedValues() {
        return checkedValues;
    }

    public void setCheckedValues(List<Integer> checkedValues) {
        this.checkedValues = checkedValues;
    }

    public List<Integer> getAvailableValues() {
        return availableValues;
    }


    public int getStrzyzenie() {
        return strzyzenie;
    }

    public int getKapiel() {
        return kapiel;
    }

    public int getSpacer() {
        return spacer;
    }

    public int getBieganie() {
        return bieganie;
    }

    public int getDieta() {
        return dieta;
    }
}
