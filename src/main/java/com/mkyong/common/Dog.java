package com.mkyong.common;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by x on 2017-07-20.
 */
@ManagedBean
@SessionScoped
public class Dog implements Serializable {
    private static final long serialVersionUID = 1L;



    private String name;
    private int age;


   // List<Dog> dogs = new ArrayList<Dog>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
