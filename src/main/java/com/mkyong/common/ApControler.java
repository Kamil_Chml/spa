package com.mkyong.common;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by x on 2017-07-23.
 */
@ManagedBean
@SessionScoped
public class ApControler implements Serializable {

    public Set<Owner> owner = new HashSet<>();
    public Set<Dog> dog = new HashSet<>();


    public Set<Owner> getOwners() {
        return owner;
    }

    public Set<Dog> getDog() {
        return dog;
    }


}
