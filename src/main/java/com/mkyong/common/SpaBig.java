package com.mkyong.common;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by x on 2017-07-20.
 */
@ManagedBean
@SessionScoped
public class SpaBig implements Serializable {

    private int strzyzenie = 80;
    private int kapiel = 50;
    private int spacer = 15;
    private int bieganie = 30;
    private int dieta = 150;

    private List<Integer> checkedValues;
    private List<Integer> availableValues;

    @PostConstruct
    public void init(){
        availableValues = new ArrayList<>();
        availableValues.add(strzyzenie);
        availableValues.add(kapiel);
        availableValues.add(spacer);
        availableValues.add(bieganie);
        availableValues.add(dieta);
        System.out.println("metoda init()" + availableValues );
    }
    public List<Integer> submit() {

        return checkedValues;
    }
    public List<Integer> getCheckedValues() {
        System.out.println("metoda getCheckedValues" + checkedValues);
        return checkedValues;
    }

    public void setCheckedValues(List<Integer> checkedValues) {
        System.out.println("seter chgeckedvalues" + checkedValues);
        this.checkedValues = checkedValues;
    }


//    public int  total (){
//
//        int[] array = checkedValues.stream().mapToInt(i->i).toArray();
//        int sum = IntStream.of(array).sum();
//        System.out.println("The sum is " + sum);
//        return sum;
//    }

    public List<Integer> getAvailableValues() {
        return availableValues;
    }

    public void setStrzyzenie(int strzyzenie) {
        this.strzyzenie = strzyzenie;
    }

    public void setKapiel(int kapiel) {
        this.kapiel = kapiel;
    }

    public void setSpacer(int spacer) {
        this.spacer = spacer;
    }

    public void setBieganie(int bieganie) {
        this.bieganie = bieganie;
    }

    public void setDieta(int dieta) {
        this.dieta = dieta;
    }

    public int getStrzyzenie() {
        return strzyzenie;
    }

    public int getKapiel() {
        return kapiel;
    }

    public int getSpacer() {
        return spacer;
    }

    public int getBieganie() {
        return bieganie;
    }

    public int getDieta() {
        return dieta;
    }

}
